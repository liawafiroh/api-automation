*** Settings ***
Library    RequestsLibrary
Library    JSONLibrary
Library    Collections


*** Variables ***
${baseUrl}          https://rc-api.rctiplus.com


*** Test Cases ***
Post Request
    create session     Login     ${baseUrl}
    &{body}=   create dictionary    username=liawafiroh@gmail.com      password=lialialia      device_id=3463784       platform=android 
    &{header}=  create dictionary   Content-Type=application/json      Authorization=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2aWQiOjAsInRva2VuIjoiNjg3OTA0OWMwOTY4YTNkMiIsInBsIjoiYW5kcm9pZCIsImRldmljZV9pZCI6IjAwYmY3NzE0LTkxMDQtMTFlYi05MDA0LWZhNzEzMDAwNGIzZSJ9.Us247clX1vbJAYgufInhdwX-hwLrvPsNaRYIBM095NE
    
    ${response} =    post request    Login    /api/v3/login   data=${body}   headers=${header}
    log to console     Response API Login
    log to console     ${response.status_code}
    log to console     ${response.content}

    # validation
    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}    200
    ${res_body}=    convert to string   ${response.content}
    should contain  ${res_body}     Success